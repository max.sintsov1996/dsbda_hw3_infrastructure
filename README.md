Data science lab2 - spark
=====
## Task

### Business logic

Program that plots using Grafana the dependence of the frequency Google requests and the value of Tesla stocks

### Delivery format

Ubuntu docker

### Storage technology

PostgreSQL

### Computation technology

Spark Streaming

### Report includes

1. Screenshots of successfully executed job and result (logs)
2. Quick build and deploy manual (commands, OS requirements etc)
3. System components communication diagram (UML or COMET)

Build and deploy
=======================

## Required tools

- docker

## Run

```
cd Docker
sudo sudo docker build -t stock_grafana .
sudo docker run -t -i -p 127.0.0.1:3000:3000 stock_grafana /bin/bash
```
## Check results

open in your browser page: http://localhost:3000  
  
login       admin  
password    654123  

## System components communication diagram

![System components communication diagram](./SystemComponentsCommunicationDiagram.png)

## System components communication diagram

![System components communication diagram](./screeshots/diagram.png)
![Screnshot of loading data](./screeshots/datastream.png)
