import psycopg2
from pytrends.request import TrendReq
import yfinance as yf
import datetime
import pytz
import time
import threading

con = psycopg2.connect(
    database="stocks_interest", 
    user="username", 
    password="password", 
    host="127.0.0.1", 
    port="5432"
    )
cur = con.cursor()

def streaming_gtrends():
    while True:
        time.sleep(3600)

        time_period_of_int_start = datetime.date.today() - datetime.timedelta(hours=1)
        time_period_of_int_end = datetime.date.today()

        TSLA_interest = pytrends.get_historical_interest(kw_list,
                                                year_start = time_period_of_int_start.year, 
                                                month_start = time_period_of_int_start.month, 
                                                day_start = time_period_of_int_start.day,
                                                hour_start = 0, 
                                                year_end = time_period_of_int_end.year, 
                                                month_end = time_period_of_int_end.month, 
                                                day_end = time_period_of_int_end.day, 
                                                hour_end = 24, 
                                                cat = 0, 
                                                geo='', 
                                                gprop='', 
                                                sleep=0).drop(columns='isPartial')

        timestamp = str(TSLA_interest['tesla stock'].keys()[-1])
        interest = str(TSLA_interest['tesla stock'].iloc[-1])
        print("get interest metric timestamp = " + timestamp + " interest = " + interest)

        cur.execute(
            "INSERT INTO interests (interest,time) VALUES (" + interest + "," + "\'"+ timestamp + "\'" + ")"
        )
        con.commit() 

def streaming_stocks():
    while True:
        time.sleep(900)
        utc = pytz.timezone('UTC')

        TSLA = yf.Ticker("TSLA")
        TSLA_hist = TSLA.history(period="1d",interval = "15m")
        timestamp = str(TSLA_hist['Close'].keys()[-1].astimezone(utc))
        price = str(TSLA_hist['Close'].iloc[-1])

        print("get price metric timestamp = " + timestamp + " price = " + interest)

        cur.execute(
            "INSERT INTO stocks (price,time) VALUES (" + price + "," + "\'"+ timestamp + "\'" + ")"
        )
        con.commit()

    

if __name__ == "__main__":

    # try get cursor for database
    print("Database opened successfully")

    # first get information of interests and pricing for the last 5 days

    # form ang get request for pytrends
    pytrends = TrendReq(hl='en-US', tz=100)
    kw_list=['tesla stock']

    time_period_of_int_start = datetime.date.today() - datetime.timedelta(days=5)
    time_period_of_int_end = datetime.date.today()

    TSLA_interest = pytrends.get_historical_interest(kw_list,
                                             year_start = time_period_of_int_start.year, 
                                             month_start = time_period_of_int_start.month, 
                                             day_start = time_period_of_int_start.day,
                                             hour_start = 0, 
                                             year_end = time_period_of_int_end.year, 
                                             month_end = time_period_of_int_end.month, 
                                             day_end = time_period_of_int_end.day, 
                                             hour_end = 0, 
                                             cat = 0, 
                                             geo='', 
                                             gprop='', 
                                             sleep=0).drop(columns='isPartial')

    # writing data of interest to database

    print(TSLA_interest['tesla stock'].keys()[0])
    print(TSLA_interest['tesla stock'].iloc[0])

    for timestamp in TSLA_interest['tesla stock'].keys():
        hourly_interest = str(TSLA_interest['tesla stock'][timestamp])
        print(str(timestamp) + " " + hourly_interest)
        cur.execute(
            "INSERT INTO interests (interest,time) VALUES (" + hourly_interest + "," + "\'"+ str(timestamp) + "\'" + ")"
        )
        con.commit()  


    # form ang get request for yfinance
    TSLA = yf.Ticker("TSLA")
    TSLA_hist = TSLA.history(period="5d",interval = "15m")
    timelist = list(TSLA_hist['Close'].keys())

    utc = pytz.timezone('UTC')

    # writing data of stocks price to database
    for timestamp in timelist:
        price = str(TSLA_hist['Close'][timestamp])
        timestamp = timestamp.astimezone(utc)
        print(timestamp, end='')
        print(" " + price)
        cur.execute(
            "INSERT INTO stocks (price,time) VALUES (" + price + "," + "\'"+ str(timestamp) + "\'" + ")"
        )
        con.commit()  

    # receiving periodic data

    gtrendsThread = threading.Thread(target=streaming_gtrends)
    gtrendsThread.start()

    stocksThread = threading.Thread(target=streaming_stocks)
    stocksThread.start()

    input("Press any key to stop applycation...")
    # con.close()
    
